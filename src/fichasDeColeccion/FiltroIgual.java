package fichasDeColeccion;

public class FiltroIgual implements Filtro{
	private String attribute;
	private float barrier;
	
	public FiltroIgual(String attribute,float barrier){
		this.attribute = attribute;
		this.barrier = barrier;
		
	}

	public boolean cumple(Ficha f){
		if(f.getAttribute(attribute) == barrier){
			return true;}
		return false;
	}

}
