package fichasDeColeccion;
import java.util.ArrayList;
import java.util.Collections;;

public class Main {

	public static void main(String[] args) {
		System.out.println("----Parte de Fichas de SuperHeroes----" + "\n");
		mostrarHeroes();
		System.out.println("\n"+"----Parte de Fichas de Automoviles----" +"\n");
		mostrarAutos();
	}
	
	private static void mostrarHeroes(){
		ArrayList<Ficha> l = new ArrayList<Ficha>();
		
		FichaSimple f1 = new FichaSimple("Flash",1.78f,90f,900f,10f,800000f);
		FichaSimple f2 = new FichaSimple("Capitan Frio",1.88f,95f,600f,20f,48f);
		FichaSimple f3 = new FichaSimple("Superman",1.92f,107f,2600f,200f,1200f);
		FichaSimple f4 = new FichaSimple("Batman",1.89f,96f,950f,15f,120f);
		FichaSimple f5 = new FichaSimple("Robin",1.75f,65f,710f,8f,60f);
		FichaSimple f6 = new FichaSimple("Guason",1.85f,72f,300f,3f,60f);
		FichaSimple f7 = new FichaSimple("Lex Luthor",1.88f,95f,1000f,1f,70f);
		FichaSimple f8 = new FichaSimple("Gatubela",1.69f,51f,720f,5f,90f);
		
		f1.setAttribute("Inteligencia",50f);
		f4.setAttribute("Inteligencia",150f);
		f5.setAttribute("Inteligencia",100f);
		f6.setAttribute("Inteligencia",150f);
		f8.setAttribute("Inteligencia",150f);
		f2.setAttribute("Maldad",50f);
		f6.setAttribute("Maldad",100f);
		f7.setAttribute("Maldad",75f);
		f8.setAttribute("Maldad",25f);
		
		FichaCompuesta g1 = new FichaCompuesta("Duo Dinamico");
		FichaCompuesta g2 = new FichaCompuesta("Liga de la Justicia");
		FichaCompuesta g3 = new FichaCompuesta("Liga de la Injusticia");
		
		g1.addFicha(f4);
		g1.addFicha(f5);
		g2.addFicha(g1);
		g2.addFicha(f1);
		g2.addFicha(f3);
		g3.addFicha(f2);
		g3.addFicha(f6);
		g3.addFicha(f7);
		g3.addFicha(f8);
		
		l.add(g1);
		l.add(g2);
		l.add(g3);
		
		for(int i = 0; i<l.size();i++){
			System.out.println(l.get(i).getNombre());
			System.out.println("fuerza: " + l.get(i).getAttribute("fuerza"));
			System.out.println("peleas_ganadas: " + l.get(i).getAttribute("peleas_ganadas"));
			System.out.println("Inteligencia: " + l.get(i).getAttribute("Inteligencia"));
			System.out.println("Maldad: " + l.get(i).getAttribute("Maldad") + "\n");
		}

		FiltroMayor filtro1 = new FiltroMayor("velocidad",100f);
		FiltroMayor filtro2 = new FiltroMayor("peso",70f);
		FiltroMayor filtro3 = new FiltroMayor("Inteligencia",80f);
		FiltroMayor filtro4 = new FiltroMayor("Maldad",50f);
		FiltroMenor filtro5 = new FiltroMenor("fuerza",900f);
		FiltroIgual filtro6 = new FiltroIgual("Maldad",50f);
		FiltroOr filtro7 = new FiltroOr(filtro6,filtro4);
		FiltroAND filtro8 = new FiltroAND(filtro3,filtro7);
		FichaCompuesta c = new FichaCompuesta("Coleccion");
		
		c.addFicha(g1);
		c.addFicha(g2);
		c.addFicha(g3);
		
		ComparadorAtributo c1 = new ComparadorAtributo("velocidad",-1);
		ComparadorAtributo c2 = new ComparadorAtributo("fuerza",1);
		ComparadorNombre c3 = new ComparadorNombre(-1);
	
		System.out.println("velocidad mayor a 100, ordenado por velocidad (descendente).");
		l= c.getCollection(filtro1);
		Collections.sort(l,c1);
		System.out.println(l.toString());

		System.out.println("fuerza menor que 900, ordenado por fuerza (ascendente).");
		l= c.getCollection(filtro5);
		Collections.sort(l,c2);
		System.out.println(l.toString());

		System.out.println("maldad mayor o igual a 50% e inteligencia mayor a 80, en cualquier orden.");
		System.out.println(c.getCollection(filtro8).toString());
		
		System.out.println("peso mayor a 70, ordenado por nombre (descendente).");
		l= c.getCollection(filtro2);
		Collections.sort(l,c3);
		System.out.println(l.toString());
	}
	
	private static void mostrarAutos(){
		
		FichaSimple f1 = new FichaSimple("Chevrolet Cruze");
		FichaSimple f2 = new FichaSimple("Chevrolet S10");
		FichaSimple f3 = new FichaSimple("Chevrolet Camaro");
		FichaSimple f4 = new FichaSimple("Ford Ka");
		FichaSimple f5 = new FichaSimple("Ford Ranger");
		FichaSimple f6 = new FichaSimple("Ford Mustang");
		FichaSimple f7 = new FichaSimple("Honda Fit");
		FichaSimple f8 = new FichaSimple("Honda Accord");
		      
		f1.setAttribute("Velocidad",220f);
		f1.setAttribute("HP",153f);
		f1.setAttribute("Cilindros",4f);
		f1.setAttribute("RPM",5000f);
		f1.setAttribute("Aceleracion",8.5f);
		f1.setAttribute("Cm3",1400f);
		f1.setAttribute("PrecioUSD",12000f);
		      
		f2.setAttribute("Velocidad",200f);
		f2.setAttribute("HP",200f);
		f2.setAttribute("Cilindros",4f);
		f2.setAttribute("RPM",3600f);
		f2.setAttribute("Aceleracion",7.5f);
		f2.setAttribute("Cm3",2800f);
		f2.setAttribute("PrecioUSD",31000f);
		      
		f3.setAttribute("Velocidad",380f);
		f3.setAttribute("HP",405f);
		f3.setAttribute("Cilindros",8f);
		f3.setAttribute("RPM",5900f);
		f3.setAttribute("Aceleracion",3f);
		f3.setAttribute("Cm3",6100f);
		      
		f4.setAttribute("Velocidad",180f);
		f4.setAttribute("HP",105f);
		f4.setAttribute("Cilindros",4f);
		f4.setAttribute("RPM",6000f);
		f4.setAttribute("Aceleracion",10f);
		f4.setAttribute("Cm3",1500f);
		f4.setAttribute("PrecioUSD",12500f);
		      
		f5.setAttribute("Velocidad",210f);
		f5.setAttribute("HP",166f);
		f5.setAttribute("Cilindros",4f);
		f5.setAttribute("RPM",5500f);
		f5.setAttribute("Aceleracion",8f);
		f5.setAttribute("Cm3",2500f);
		f5.setAttribute("PrecioUSD",28000f);
		     
		f6.setAttribute("Velocidad",450f);
		f6.setAttribute("HP",503f);
		f6.setAttribute("Cilindros",8f);
		f6.setAttribute("RPM",7000f);
		f6.setAttribute("Aceleracion",4f);
		f6.setAttribute("PrecioUSD",95000f);
		
		f7.setAttribute("Velocidad",230f);
		f7.setAttribute("HP",132f);
		f7.setAttribute("Cilindros",4f);
		f7.setAttribute("RPM",6600f);
		f7.setAttribute("Aceleracion",7.5f);
		f7.setAttribute("Cm3",1500f);
		f7.setAttribute("PrecioUSD",24000f);
		     
		f8.setAttribute("Velocidad",320f);
		f8.setAttribute("HP",280f);
		f8.setAttribute("Cilindros",6f);
		f8.setAttribute("RPM",6600f);
		f8.setAttribute("Aceleracion",5f);
		f8.setAttribute("Cm3",3500f);

		FichaCompuesta g1 = new FichaCompuesta("Japoneses");
		FichaCompuesta g2 = new FichaCompuesta("Sedan");
		FichaCompuesta g3 = new FichaCompuesta("Carrera");
		FichaCompuesta g4 = new FichaCompuesta("Camionetas");
		FichaCompuesta g5 = new FichaCompuesta("Potencia");
		
		ArrayList<Ficha> l = new ArrayList<Ficha>();
		
		l.add(g1);
		l.add(g2);
		l.add(g3);
		l.add(g4);
		l.add(g5);
		
		g1.addFicha(f7);
		g1.addFicha(f8);
		g2.addFicha(f1);
		g2.addFicha(f4);
		g2.addFicha(g1);
		g3.addFicha(f3);
		g3.addFicha(f6);
		g4.addFicha(f2);
		g4.addFicha(f5);
		g5.addFicha(g3);
		g5.addFicha(g4);
		
		for(int i=0;i<l.size();i++){
			System.out.println(l.get(i).getNombre());
			System.out.println("Velocidad: " + l.get(i).getAttribute("Velocidad"));
			System.out.println("Aceleracion: " + l.get(i).getAttribute("Aceleracion"));
			System.out.println("Precio en Dolares: " + l.get(i).getAttribute("PrecioUSD") + "\n");
		}
		
		FichaCompuesta c = new FichaCompuesta("Coleccion");
		
		c.addFicha(g1);
		c.addFicha(g2);
		c.addFicha(g3);
		c.addFicha(g4);
		c.addFicha(g5);
		
		FiltroMayor filtro1 = new FiltroMayor("Velocidad",200f);
		FiltroMenor filtro2 = new FiltroMenor("Cilindros",2000f);
		FiltroMayor filtro3 = new FiltroMayor("HP",150f);
		FiltroMayor filtro4 = new FiltroMayor("PrecioUSD",15000f);
		FiltroAND filtro5 = new FiltroAND(filtro3,filtro4);
		
		ComparadorAtributo c1 = new ComparadorAtributo("Velocidad",-1);
		ComparadorAtributo c2 = new ComparadorAtributo("Cilindros",1);
	
		System.out.println("autos con velocidad mayor a 200, ordenado por velocidad (descendente).");
		l= c.getCollection(filtro1);
		Collections.sort(l,c1);
		System.out.println(l.toString());

		System.out.println("cilindrada menor que 2000, ordenado por cilindrada (ascendente).");
		l= c.getCollection(filtro2);
		Collections.sort(l,c2);
		System.out.println(l.toString());

		System.out.println("HP mayor a 150 y precio mayor a 15000 USD, en cualquier orden.");
		System.out.println(c.getCollection(filtro5).toString());
			
	}
}
