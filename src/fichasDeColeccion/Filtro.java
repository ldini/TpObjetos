package fichasDeColeccion;

public interface Filtro {

	boolean cumple(Ficha f);

}
