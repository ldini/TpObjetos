package fichasDeColeccion;

import java.util.Comparator;

public interface Comparador extends Comparator<Ficha>{
	public int compare(Ficha f1, Ficha f2);
}