package fichasDeColeccion;
import java.util.ArrayList;

public class FichaCompuesta extends Ficha{
	private ArrayList<Ficha> e;
	
	public FichaCompuesta(String nombre) {
		super(nombre);
		e = new ArrayList<Ficha>();
		
	}
	
	public void addFicha(Ficha f){
		e.add(f);
	}
	
	public Float getAttribute(String atributo) {
		float c = 0;
		for(int i=0;i<e.size();i++) {
			c = c + e.get(i).getAttribute(atributo);
		}
		if(e.size() == 0)
			return 0f;
		return c/e.size();
	}


	public ArrayList<Ficha> getCollection(Filtro filtro) {
		ArrayList<Ficha> l = new ArrayList<Ficha>();
		ArrayList<Ficha> aux = new ArrayList<Ficha>();
		for(int i=0;i<e.size();i++){
			aux = e.get(i).getCollection(filtro);
			if(aux != null)
				this.union(l,aux);
		}
		return l;
	}

	public void copy(Ficha f) {
		f.setNombre(this.getNombre());
		for(int i=0;i<e.size();i++){
			e.get(i).copy(f);
		}
	}
	
	public ArrayList<String> getAllAttributes(){
		ArrayList<String> l1 = new ArrayList<String>();
		ArrayList<String> l2 = new ArrayList<String>();
		for(int i=0;i<e.size();i++) {
			l1 = e.get(i).getAllAttributes();
			for (int j=0;j<l1.size();j++){
				if(!l2.contains(l1.get(j)))
					l2.add(l1.get(j));
			}
		}
		return l2;
	}
	
	
	public String toString(){
		StringBuffer message = new StringBuffer();
		message.append("Nombre: "+this.getNombre()+"\n"+"\n");
		ArrayList<String> l = new ArrayList<String>();
		l = this.getAllAttributes();
		for(int i=0;i<l.size();i++){
			message.append(l.get(i)+": "+this.getAttribute(l.get(i))+"\n");
		}
		return message.toString();
	}
	
	private void union(ArrayList<Ficha> l1,ArrayList<Ficha> l2){
		for(int j=0;j<l2.size();j++){
			if(!l1.contains(l2.get(j)))
					l1.add(l2.get(j));			
		}
	}
}
