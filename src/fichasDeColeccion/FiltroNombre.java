package fichasDeColeccion;

public class FiltroNombre implements Filtro{
	private String nombre;
	
	public FiltroNombre(String nombre){
		this.nombre = nombre;
	}

	public boolean cumple(Ficha f){
		if(f.getNombre() == nombre){
			return true;}
		return false;
	}
}
