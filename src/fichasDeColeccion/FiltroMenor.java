package fichasDeColeccion;

public class FiltroMenor implements Filtro{
	private String attribute;
	private float barrier;
	
	public FiltroMenor(String attribute,float barrier){
		this.attribute = attribute;
		this.barrier = barrier;
		
	}

	public boolean cumple(Ficha f){
		if(f.getAttribute(attribute) < barrier){
			return true;}
		return false;
	}

}
