package fichasDeColeccion;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

public class FichaSimple extends Ficha {

	private Hashtable<String,Float> h;
   public static final String NOMBRE="nombre";
	public static final String ALTURA="altura";
    public static final String PESO="peso";
    public static final String FUERZA="fuerza";
    public static final String PELEAS_GANADAS="peleas_ganadas";
    public static final String VELOCIDAD="velocidad";
	
	public FichaSimple (String nombre,Float altura,Float peso, Float fuerza, Float peleas_ganadas,Float velocidad){
		super(nombre);
		h = new Hashtable<String,Float>();
		h.put(ALTURA,altura);
		h.put(PESO,peso);
		h.put(FUERZA,fuerza);
		h.put(PELEAS_GANADAS,peleas_ganadas);
		h.put(VELOCIDAD,velocidad);
	}
	
	public FichaSimple(String nombre){
		super(nombre);
		h = new Hashtable<String,Float>();
	}
	
	public Float getAttribute(String attribute){
		if(h.containsKey(attribute))
			return h.get(attribute);
		return 0f;
	}
	
	public void setAttribute(String attribute,Float value){ 
		h.put(attribute,value);		
	}
	
	public void copy(Ficha f){
		f.setNombre(this.getNombre());
		Enumeration<String> e = h.keys();
		String attribute;
		Float value;
		while( e.hasMoreElements() ){
			attribute = e.nextElement();
			value = h.get(attribute);
		  ((FichaSimple)f).setAttribute(attribute, value);
		}
	}
	
	public ArrayList<Ficha> getCollection(Filtro filtro){
		if(filtro.cumple(this)){
			FichaSimple ficha = new FichaSimple(this.getNombre());
			ArrayList<Ficha> l = new ArrayList<Ficha>();
			this.copy(ficha);
			l.add(ficha);
			return l;
		}
		return null;
	}
	
	public String toString(){
		StringBuffer message = new StringBuffer();
		message.append("Nombre: "+this.getNombre()+"\n");
		Enumeration<String> e = h.keys();
		String attribute;
		Float value;
		while( e.hasMoreElements() ){
			attribute = e.nextElement();
			value = h.get(attribute);
			message.append(attribute);
			message.append(": ");
			message.append(value);
			message.append("\n");
		}
		message.append("\n");
		return message.toString();
	}

	public ArrayList<String> getAllAttributes() {
		ArrayList<String> l = new ArrayList<String>();
		Enumeration<String> e = h.keys();
		while( e.hasMoreElements() ){
			l.add(e.nextElement());
		}
		return l;
	}
	
	public boolean equals(Object o1){
		FichaSimple fs= (FichaSimple)o1;
		return this.getNombre().equals(fs.getNombre());
	}
}