package fichasDeColeccion;

public class FiltroAND extends FiltroCompuesto{
	
	Filtro f1;
	Filtro f2;
	
	public FiltroAND(Filtro f1,Filtro f2){
		this.f1 = f1;
		this.f2 = f2;
	}

	public boolean cumple(Ficha f) {
		if(f1.cumple(f) && f2.cumple(f))
			return true;
		return false;
	}
	
	

}
