package fichasDeColeccion;

import java.util.ArrayList;

public abstract class Ficha {
	
	private String nombre;
	
	public Ficha(String nombre){
		this.nombre = nombre;
	}
	
	public String getNombre(){
		return this.nombre;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public abstract Float getAttribute(String atributo);
	public abstract ArrayList<Ficha> getCollection(Filtro f);
	public abstract void copy(Ficha f); 
	public abstract String toString();
	public abstract ArrayList<String> getAllAttributes();
	
}