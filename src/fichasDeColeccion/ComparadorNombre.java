package fichasDeColeccion;

public class ComparadorNombre implements Comparador{
	public static final int REVERSA=-1;
	public static final int SIMPLE = 1;
	Comparador sig;
	int sentido=1;

	public ComparadorNombre(int sentido){
		this.sentido = sentido;
	}
	
	public ComparadorNombre(Comparador sig, int sentido){
		this.sig=sig;
		this.sentido = sentido;
	}
	
	public int compare(Ficha f1, Ficha f2) {
		int aux = f1.getNombre().compareTo(f2.getNombre());
		if (aux == 0 && sig!=null)
			return sig.compare(f1, f2)*sentido;
		return aux*sentido;
	}

}
