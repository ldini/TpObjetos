package fichasDeColeccion;

public abstract class FiltroCompuesto  implements Filtro{
	public abstract boolean cumple(Ficha f);
}