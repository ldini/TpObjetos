package fichasDeColeccion;

public class ComparadorAtributo implements Comparador{
	
	private String attribute;
	public static final int REVERSA=-1;
	public static final int SIMPLE = 1;
	Comparador sig;
	int sentido=1;

	public ComparadorAtributo(String attribute){
		this.attribute = attribute;
	}

	public ComparadorAtributo(String attribute, int sentido){
		this.attribute = attribute;
		this.sentido = sentido;
	}
	
	public ComparadorAtributo(String attribute, Comparador sig, int sentido){
		this.attribute = attribute;
		this.sig=sig;
		this.sentido = sentido;
	}
	
	@Override
	public int compare(Ficha f1, Ficha f2) {
		int aux = f1.getAttribute(attribute).compareTo(f2.getAttribute(attribute));
		if (aux == 0 && sig!=null)
			return sig.compare(f1, f2)*sentido;
		return aux*sentido;
	}

}
